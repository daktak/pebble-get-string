#include <pebble.h>
#include <pebble-events/pebble-events.h>
#include "pebble-get-string.h"

static GetStringCallback *s_callback;
static GetStringStatus s_status;
static char *s_value;

static char s_url[32];

static EventHandle s_event_handle;

static void inbox_received_handler(DictionaryIterator *iter, void *context) {
  Tuple *reply_tuple = dict_find(iter, MESSAGE_KEY_GS_REPLY);
  if (reply_tuple)
  {
    Tuple *value = dict_find(iter, MESSAGE_KEY_GS_VALUE);
    strncpy(s_value, value->value->cstring, GET_STRING_BUFFER_SIZE);

    s_status = GetStringStatusAvailable;
    s_callback(s_value, s_status);
  }
}

static void fail_and_callback() {
  s_status = GetStringStatusFailed;
  s_callback(s_value, s_status);
}

static bool fetch() {
   DictionaryIterator *out;
   AppMessageResult result = app_message_outbox_begin(&out);
   if(result != APP_MSG_OK) {
     fail_and_callback();
     return false;
   }

   dict_write_uint8(out, MESSAGE_KEY_GS_REQUEST, 1);

   if(strlen(s_url)>0) {
     dict_write_cstring(out, MESSAGE_KEY_GS_URL, s_url);
   }
   result = app_message_outbox_send();
   if(result != APP_MSG_OK) {
     fail_and_callback();
     return false;
   }

   s_status = GetStringStatusPending;
   s_callback(s_value, s_status);
   return true;
}

void get_string_init() {
  if(s_value) {
    free(s_value);
  }
  s_url[0] = 0;
  s_event_handle = events_app_message_register_inbox_received(inbox_received_handler, NULL);
}

void get_string_set_url(const char *url){
  if(!url) {
    s_url[0] = 0;
  }
  else {
    strncpy(s_url, url, sizeof(s_url));
  }
}

bool get_string_fetch(GetStringCallback *callback) {
  if(!s_value) {
    return false;
  }

  if(!callback) {
    return false;
  }

  s_callback = callback;

  if(!bluetooth_connection_service_peek()) {
    s_status = GetStringStatusBluetoothDisconnected;
    s_callback(s_value, s_status);
    return false;
  }

  return fetch();
}

void get_string_deinit() {
  if(s_value) {
    free(s_value);
    s_value = NULL;
    s_callback = NULL;
    events_app_message_unsubscribe(s_event_handle);
  }
}

char* get_string_peek() {
  if(!s_value) {
    return NULL;
  }

  return s_value;
}
/*
void get_string_save(const uint32_t key) {
  if(!s_value) {
    return;
  }

  persist_write_date(key, s_value, sizeof(s_value));
}

void get_string_load(const uint32_t key) {
  if(!s_value) {
    return;
  }

  if(persist_exists(key)) {
    persist_read(key, s_value, sizeof(s_value));
  }
}
*/
