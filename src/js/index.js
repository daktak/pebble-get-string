var GetString = function() {
  this._url = '';

  this._xhrWrapper = function(url, type, callback, customHeaderKey, customHeaderValue) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function () {
      callback(xhr);
    };
    xhr.open(type, url);

    if (customHeaderKey && customHeaderValue) {
      try {xhr.setRequestHeader(customHeaderKey, customHeaderValue);} catch(e){}
    }

    xhr.send();
  };

  this._getString = function(url) {
    console.log('get-sting: '+url)
    this._xhrWrapper(url, 'GET', function(req)) {
      if (req.status == 200) {
        Pebble.sendAppMessage({
          'GS_REPLY': 1,
          'GS_VALUE': req.response
        });
      } else {
        console.log('get-sting: error fetching data (HTTP Status: '+req.status +')');
      }
    }
  }

  this.appMessageHandler = function(dict, options) {
    if(dict.payload['GS_REQUEST']) {
      console.log('get-string')
      if(options && 'url' in options){
        this._url = options['url'];
      }
      else if(dict.payload && 'GS_URL' in dict.payload) {
        this._url = dict.payload['GS_URL'];
      }
    }
  }
}

module.exports = GetString;
