#pragma once

#include <pebble.h>

#define GET_STRING_BUFFER_SIZE 32

typedef enum {
  GetStringStatusNotYetFetched = 0,
  GetStringStatusBluetoothDisconnected,
  GetStringStatusPending,
  GetStringStatusFailed,
  GetStringStatusAvailable
} GetStringStatus;

typedef void(GetStringCallback)(char *value, GetStringStatus status);

void get_string_init();

void get_string_set_url(const char *url);

bool get_string_fetch(GetStringCallback *callback);

void get_string_deinit();

char* get_string_peek();
/*
void get_string_save(const uint32_t key);

void get_string_load(const uint32_t key);
*/
